require 'test_helper'

class RiskClassificationsControllerTest < ActionController::TestCase
  setup do
    @risk_classification = risk_classifications(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:risk_classifications)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create risk_classification" do
    assert_difference('RiskClassification.count') do
      post :create, risk_classification: { Classification_Type: @risk_classification.Classification_Type, Classification_criteria: @risk_classification.Classification_criteria, Name: @risk_classification.Name, value: @risk_classification.value }
    end

    assert_redirected_to risk_classification_path(assigns(:risk_classification))
  end

  test "should show risk_classification" do
    get :show, id: @risk_classification
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @risk_classification
    assert_response :success
  end

  test "should update risk_classification" do
    put :update, id: @risk_classification, risk_classification: { Classification_Type: @risk_classification.Classification_Type, Classification_criteria: @risk_classification.Classification_criteria, Name: @risk_classification.Name, value: @risk_classification.value }
    assert_redirected_to risk_classification_path(assigns(:risk_classification))
  end

  test "should destroy risk_classification" do
    assert_difference('RiskClassification.count', -1) do
      delete :destroy, id: @risk_classification
    end

    assert_redirected_to risk_classifications_path
  end
end
