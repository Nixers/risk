class CreateBuRiskMgts < ActiveRecord::Migration
  def change
    create_table :bu_risk_mgts do |t|
      t.string :Risk_Title
      t.string :Applicable_Business_Units
      t.string :What_is_the_business_impact_if_this_threats_materializes
      t.string :Threats
      t.string :Vulnerabilities
      t.string :Risk_Classification
      t.integer :Risk_score
      t.string :Risk_Mitigation_Strategy
      t.string :Compensating_Continuity_Plans
      t.integer :Risk_Residual_Score
      t.string :Applicable_Risk_Exceptions
      t.date :Risk_Review_periodicity

      t.timestamps
    end
  end
end
