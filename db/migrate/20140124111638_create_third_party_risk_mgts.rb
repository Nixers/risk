class CreateThirdPartyRiskMgts < ActiveRecord::Migration
  def change
    create_table :third_party_risk_mgts do |t|
      t.string :Risk_Title
      t.string :Applicable_Third_Parties
      t.string :Applicable_Assets
      t.string :Why_are_assets_shared_with_this_TP
      t.string :How_it_will_be_controled
      t.string :Threats
      t.string :Vulnerabilities
      t.string :Risk_Classification
      t.integer :Risk_Score
      t.string :Risk_Mitigation_Strategy
      t.string :Compensating_Controls
      t.integer :Risk_Residual_Score
      t.string :Applicable_Risk_Exceptions
      t.date :Risk_Review_Periodicity

      t.timestamps
    end
  end
end
