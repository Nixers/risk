# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140127125900) do

  create_table "asset_risk_mgts", :force => true do |t|
    t.string   "Risk_Title"
    t.string   "Applicable_Assets"
    t.string   "Threats"
    t.string   "Vulnerabilities"
    t.string   "Risk_Classification"
    t.integer  "Risk_Score"
    t.string   "Risk_Mitigation_Strategy"
    t.string   "Compensating_Controls"
    t.integer  "Risk_Residual_Score"
    t.string   "Applicable_Risk_Exceptions"
    t.date     "Risk_Review_periodicity"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "bu_risk_mgts", :force => true do |t|
    t.string   "Risk_Title"
    t.string   "Applicable_Business_Units"
    t.string   "What_is_the_business_impact_if_this_threats_materializes"
    t.string   "Threats"
    t.string   "Vulnerabilities"
    t.string   "Risk_Classification"
    t.integer  "Risk_score"
    t.string   "Risk_Mitigation_Strategy"
    t.string   "Compensating_Continuity_Plans"
    t.integer  "Risk_Residual_Score"
    t.string   "Applicable_Risk_Exceptions"
    t.date     "Risk_Review_periodicity"
    t.datetime "created_at",                                               :null => false
    t.datetime "updated_at",                                               :null => false
  end

  create_table "businessunits", :force => true do |t|
    t.string   "b_name"
    t.string   "b_site"
    t.string   "b_area"
    t.string   "b_city"
    t.string   "b_state"
    t.string   "b_country"
    t.string   "time_zone"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "dashboards", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "documents", :force => true do |t|
    t.string   "title"
    t.string   "location"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "members", :force => true do |t|
    t.string   "name"
    t.integer  "age"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "policies", :force => true do |t|
    t.string   "p_name"
    t.string   "p_code"
    t.string   "p_scope"
    t.string   "p_objective"
    t.string   "p_description"
    t.integer  "businessunit_id"
    t.integer  "document_id"
    t.integer  "user_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "attach_file_name"
    t.string   "attach_content_type"
    t.integer  "attach_file_size"
    t.datetime "attach_updated_at"
  end

  create_table "risk_classifications", :force => true do |t|
    t.string   "Classification_Type"
    t.string   "Name"
    t.string   "Classification_criteria"
    t.integer  "value"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "risk_exceptions", :force => true do |t|
    t.string   "Risk_Exception_Title"
    t.string   "Description"
    t.string   "Author"
    t.date     "Expiration"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "risks", :force => true do |t|
    t.string   "Classification"
    t.string   "Type"
    t.string   "Name"
    t.string   "Criteria"
    t.integer  "Value"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "third_party_risk_mgts", :force => true do |t|
    t.string   "Risk_Title"
    t.string   "Applicable_Third_Parties"
    t.string   "Applicable_Assets"
    t.string   "Why_are_assets_shared_with_this_TP"
    t.string   "How_it_will_be_controled"
    t.string   "Threats"
    t.string   "Vulnerabilities"
    t.string   "Risk_Classification"
    t.integer  "Risk_Score"
    t.string   "Risk_Mitigation_Strategy"
    t.string   "Compensating_Controls"
    t.integer  "Risk_Residual_Score"
    t.string   "Applicable_Risk_Exceptions"
    t.date     "Risk_Review_Periodicity"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
