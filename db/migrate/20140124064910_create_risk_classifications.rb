class CreateRiskClassifications < ActiveRecord::Migration
  def change
    create_table :risk_classifications do |t|
      t.string :Classification_Type
      t.string :Name
      t.string :Classification_criteria
      t.integer :value

      t.timestamps
    end
  end
end
