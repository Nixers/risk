require 'test_helper'

class BuRiskMgtsControllerTest < ActionController::TestCase
  setup do
    @bu_risk_mgt = bu_risk_mgts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bu_risk_mgts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bu_risk_mgt" do
    assert_difference('BuRiskMgt.count') do
      post :create, bu_risk_mgt: { Applicable_Business_Units: @bu_risk_mgt.Applicable_Business_Units, Applicable_Risk_Exceptions: @bu_risk_mgt.Applicable_Risk_Exceptions, Compensating_Continuity_Plans: @bu_risk_mgt.Compensating_Continuity_Plans, Risk_Classification: @bu_risk_mgt.Risk_Classification, Risk_Mitigation_Strategy: @bu_risk_mgt.Risk_Mitigation_Strategy, Risk_Residual_Score: @bu_risk_mgt.Risk_Residual_Score, Risk_Review_periodicity: @bu_risk_mgt.Risk_Review_periodicity, Risk_Title: @bu_risk_mgt.Risk_Title, Risk_score: @bu_risk_mgt.Risk_score, Threats: @bu_risk_mgt.Threats, Vulnerabilities: @bu_risk_mgt.Vulnerabilities, What_is_the_business_impact_if_this_threats_materializes: @bu_risk_mgt.What_is_the_business_impact_if_this_threats_materializes }
    end

    assert_redirected_to bu_risk_mgt_path(assigns(:bu_risk_mgt))
  end

  test "should show bu_risk_mgt" do
    get :show, id: @bu_risk_mgt
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bu_risk_mgt
    assert_response :success
  end

  test "should update bu_risk_mgt" do
    put :update, id: @bu_risk_mgt, bu_risk_mgt: { Applicable_Business_Units: @bu_risk_mgt.Applicable_Business_Units, Applicable_Risk_Exceptions: @bu_risk_mgt.Applicable_Risk_Exceptions, Compensating_Continuity_Plans: @bu_risk_mgt.Compensating_Continuity_Plans, Risk_Classification: @bu_risk_mgt.Risk_Classification, Risk_Mitigation_Strategy: @bu_risk_mgt.Risk_Mitigation_Strategy, Risk_Residual_Score: @bu_risk_mgt.Risk_Residual_Score, Risk_Review_periodicity: @bu_risk_mgt.Risk_Review_periodicity, Risk_Title: @bu_risk_mgt.Risk_Title, Risk_score: @bu_risk_mgt.Risk_score, Threats: @bu_risk_mgt.Threats, Vulnerabilities: @bu_risk_mgt.Vulnerabilities, What_is_the_business_impact_if_this_threats_materializes: @bu_risk_mgt.What_is_the_business_impact_if_this_threats_materializes }
    assert_redirected_to bu_risk_mgt_path(assigns(:bu_risk_mgt))
  end

  test "should destroy bu_risk_mgt" do
    assert_difference('BuRiskMgt.count', -1) do
      delete :destroy, id: @bu_risk_mgt
    end

    assert_redirected_to bu_risk_mgts_path
  end
end
