require 'test_helper'

class RiskExceptionsControllerTest < ActionController::TestCase
  setup do
    @risk_exception = risk_exceptions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:risk_exceptions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create risk_exception" do
    assert_difference('RiskException.count') do
      post :create, risk_exception: { Author: @risk_exception.Author, Description: @risk_exception.Description, Expiration: @risk_exception.Expiration, Risk_Exception_Title: @risk_exception.Risk_Exception_Title }
    end

    assert_redirected_to risk_exception_path(assigns(:risk_exception))
  end

  test "should show risk_exception" do
    get :show, id: @risk_exception
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @risk_exception
    assert_response :success
  end

  test "should update risk_exception" do
    put :update, id: @risk_exception, risk_exception: { Author: @risk_exception.Author, Description: @risk_exception.Description, Expiration: @risk_exception.Expiration, Risk_Exception_Title: @risk_exception.Risk_Exception_Title }
    assert_redirected_to risk_exception_path(assigns(:risk_exception))
  end

  test "should destroy risk_exception" do
    assert_difference('RiskException.count', -1) do
      delete :destroy, id: @risk_exception
    end

    assert_redirected_to risk_exceptions_path
  end
end
