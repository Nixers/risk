class CreateRiskExceptions < ActiveRecord::Migration
  def change
    create_table :risk_exceptions do |t|
      t.string :Risk_Exception_Title
      t.string :Description
      t.string :Author
      t.date :Expiration

      t.timestamps
    end
  end
end
