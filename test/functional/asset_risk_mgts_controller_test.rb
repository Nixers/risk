require 'test_helper'

class AssetRiskMgtsControllerTest < ActionController::TestCase
  setup do
    @asset_risk_mgt = asset_risk_mgts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:asset_risk_mgts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create asset_risk_mgt" do
    assert_difference('AssetRiskMgt.count') do
      post :create, asset_risk_mgt: { Applicable_Assets: @asset_risk_mgt.Applicable_Assets, Applicable_Risk_Exceptions: @asset_risk_mgt.Applicable_Risk_Exceptions, Compensating_Controls: @asset_risk_mgt.Compensating_Controls, Risk_Classification: @asset_risk_mgt.Risk_Classification, Risk_Mitigation_Strategy: @asset_risk_mgt.Risk_Mitigation_Strategy, Risk_Residual_Score: @asset_risk_mgt.Risk_Residual_Score, Risk_Review_periodicity: @asset_risk_mgt.Risk_Review_periodicity, Risk_Score: @asset_risk_mgt.Risk_Score, Risk_Title: @asset_risk_mgt.Risk_Title, Threats: @asset_risk_mgt.Threats, Vulnerabilities: @asset_risk_mgt.Vulnerabilities }
    end

    assert_redirected_to asset_risk_mgt_path(assigns(:asset_risk_mgt))
  end

  test "should show asset_risk_mgt" do
    get :show, id: @asset_risk_mgt
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @asset_risk_mgt
    assert_response :success
  end

  test "should update asset_risk_mgt" do
    put :update, id: @asset_risk_mgt, asset_risk_mgt: { Applicable_Assets: @asset_risk_mgt.Applicable_Assets, Applicable_Risk_Exceptions: @asset_risk_mgt.Applicable_Risk_Exceptions, Compensating_Controls: @asset_risk_mgt.Compensating_Controls, Risk_Classification: @asset_risk_mgt.Risk_Classification, Risk_Mitigation_Strategy: @asset_risk_mgt.Risk_Mitigation_Strategy, Risk_Residual_Score: @asset_risk_mgt.Risk_Residual_Score, Risk_Review_periodicity: @asset_risk_mgt.Risk_Review_periodicity, Risk_Score: @asset_risk_mgt.Risk_Score, Risk_Title: @asset_risk_mgt.Risk_Title, Threats: @asset_risk_mgt.Threats, Vulnerabilities: @asset_risk_mgt.Vulnerabilities }
    assert_redirected_to asset_risk_mgt_path(assigns(:asset_risk_mgt))
  end

  test "should destroy asset_risk_mgt" do
    assert_difference('AssetRiskMgt.count', -1) do
      delete :destroy, id: @asset_risk_mgt
    end

    assert_redirected_to asset_risk_mgts_path
  end
end
