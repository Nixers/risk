class ThirdPartyRiskMgt < ActiveRecord::Base
  attr_accessible :Applicable_Assets, :Applicable_Risk_Exceptions, :Applicable_Third_Parties, :Compensating_Controls, :How_it_will_be_controled, :Risk_Classification, :Risk_Mitigation_Strategy, :Risk_Residual_Score, :Risk_Review_Periodicity, :Risk_Score, :Risk_Title, :Threats, :Vulnerabilities, :Why_are_assets_shared_with_this_TP
end
