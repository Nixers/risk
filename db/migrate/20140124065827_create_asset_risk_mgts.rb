class CreateAssetRiskMgts < ActiveRecord::Migration
  def change
    create_table :asset_risk_mgts do |t|
      t.string :Risk_Title
      t.string :Applicable_Assets
      t.string :Threats
      t.string :Vulnerabilities
      t.string :Risk_Classification
      t.integer :Risk_Score
      t.string :Risk_Mitigation_Strategy
      t.string :Compensating_Controls
      t.integer :Risk_Residual_Score
      t.string :Applicable_Risk_Exceptions
      t.string :Risk_Review_periodicity

      t.timestamps
    end
  end
end
