require 'test_helper'

class ThirdPartyRiskMgtsControllerTest < ActionController::TestCase
  setup do
    @third_party_risk_mgt = third_party_risk_mgts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:third_party_risk_mgts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create third_party_risk_mgt" do
    assert_difference('ThirdPartyRiskMgt.count') do
      post :create, third_party_risk_mgt: { Applicable_Assets: @third_party_risk_mgt.Applicable_Assets, Applicable_Risk_Exceptions: @third_party_risk_mgt.Applicable_Risk_Exceptions, Applicable_Third_Parties: @third_party_risk_mgt.Applicable_Third_Parties, Compensating_Controls: @third_party_risk_mgt.Compensating_Controls, How_it_will_be_controled: @third_party_risk_mgt.How_it_will_be_controled, Risk_Classification: @third_party_risk_mgt.Risk_Classification, Risk_Mitigation_Strategy: @third_party_risk_mgt.Risk_Mitigation_Strategy, Risk_Residual_Score: @third_party_risk_mgt.Risk_Residual_Score, Risk_Review_Periodicity: @third_party_risk_mgt.Risk_Review_Periodicity, Risk_Score: @third_party_risk_mgt.Risk_Score, Risk_Title: @third_party_risk_mgt.Risk_Title, Threats: @third_party_risk_mgt.Threats, Vulnerabilities: @third_party_risk_mgt.Vulnerabilities, Why_are_assets_shared_with_this_TP: @third_party_risk_mgt.Why_are_assets_shared_with_this_TP }
    end

    assert_redirected_to third_party_risk_mgt_path(assigns(:third_party_risk_mgt))
  end

  test "should show third_party_risk_mgt" do
    get :show, id: @third_party_risk_mgt
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @third_party_risk_mgt
    assert_response :success
  end

  test "should update third_party_risk_mgt" do
    put :update, id: @third_party_risk_mgt, third_party_risk_mgt: { Applicable_Assets: @third_party_risk_mgt.Applicable_Assets, Applicable_Risk_Exceptions: @third_party_risk_mgt.Applicable_Risk_Exceptions, Applicable_Third_Parties: @third_party_risk_mgt.Applicable_Third_Parties, Compensating_Controls: @third_party_risk_mgt.Compensating_Controls, How_it_will_be_controled: @third_party_risk_mgt.How_it_will_be_controled, Risk_Classification: @third_party_risk_mgt.Risk_Classification, Risk_Mitigation_Strategy: @third_party_risk_mgt.Risk_Mitigation_Strategy, Risk_Residual_Score: @third_party_risk_mgt.Risk_Residual_Score, Risk_Review_Periodicity: @third_party_risk_mgt.Risk_Review_Periodicity, Risk_Score: @third_party_risk_mgt.Risk_Score, Risk_Title: @third_party_risk_mgt.Risk_Title, Threats: @third_party_risk_mgt.Threats, Vulnerabilities: @third_party_risk_mgt.Vulnerabilities, Why_are_assets_shared_with_this_TP: @third_party_risk_mgt.Why_are_assets_shared_with_this_TP }
    assert_redirected_to third_party_risk_mgt_path(assigns(:third_party_risk_mgt))
  end

  test "should destroy third_party_risk_mgt" do
    assert_difference('ThirdPartyRiskMgt.count', -1) do
      delete :destroy, id: @third_party_risk_mgt
    end

    assert_redirected_to third_party_risk_mgts_path
  end
end
